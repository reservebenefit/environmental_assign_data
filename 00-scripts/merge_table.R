chlorotab=read.csv("03-results/all_CHECKED_chlorophyll.tsv",sep="\t",header=T) 
salt_temp=read.csv("03-results/all_depth_repro_CORRECTED_temperature_salinity.csv",sep="\t",header=T)

scratch=merge(chlorotab,salt_temp,by="labels")
scratch$species[which(scratch$species =="Mul")]="mul"
scratch$species[which(scratch$species =="Ser")]="ser"

## LABELS tag
labels=c(as.character(scratch[which(scratch$species=="dip"),]$labels),
	as.character(scratch[which(scratch$species=="mul"),]$labels),
	as.character(scratch[which(scratch$species=="pan"),]$labels),
	as.character(scratch[which(scratch$species=="ser"),]$labels))

## SPECIES tag
species=c(as.character(scratch[which(scratch$species=="dip"),]$species),
	as.character(scratch[which(scratch$species=="mul"),]$species),
	as.character(scratch[which(scratch$species=="pan"),]$species),
	as.character(scratch[which(scratch$species=="ser"),]$species))

## SALINITY surface whole year
### diplodus salinity surface whole year
dip_salinity_surface_whole_yr_max=scratch[which(scratch$species=="dip"),]$salinity_surface_whole_year_max
dip_salinity_surface_whole_yr_mean=scratch[which(scratch$species=="dip"),]$salinity_surface_whole_year_mean
dip_salinity_surface_whole_yr_min=scratch[which(scratch$species=="dip"),]$salinity_surface_whole_year_min
### mullus salinity surface whole year
mul_salinity_surface_whole_yr_max=scratch[which(scratch$species=="mul"),]$salinity_surface_whole_year_max
mul_salinity_surface_whole_yr_mean=scratch[which(scratch$species=="mul"),]$salinity_surface_whole_year_mean
mul_salinity_surface_whole_yr_min=scratch[which(scratch$species=="mul"),]$salinity_surface_whole_year_min
### serran salinity surface whole year
ser_salinity_surface_whole_yr_max=scratch[which(scratch$species=="ser"),]$salinity_surface_whole_year_max
ser_salinity_surface_whole_yr_mean=scratch[which(scratch$species=="ser"),]$salinity_surface_whole_year_mean
ser_salinity_surface_whole_yr_min=scratch[which(scratch$species=="ser"),]$salinity_surface_whole_year_min
### pan salinity surface whole year
pan_salinity_surface_whole_yr_max=scratch[which(scratch$species=="pan"),]$salinity_surface_whole_year_max
pan_salinity_surface_whole_yr_mean=scratch[which(scratch$species=="pan"),]$salinity_surface_whole_year_mean
pan_salinity_surface_whole_yr_min=scratch[which(scratch$species=="pan"),]$salinity_surface_whole_year_min
### ALL salinity surface whole year
salinity_surface_whole_yr_max=c(dip_salinity_surface_whole_yr_max,
	mul_salinity_surface_whole_yr_max,
	pan_salinity_surface_whole_yr_max,
	ser_salinity_surface_whole_yr_max)
salinity_surface_whole_yr_mean=c(dip_salinity_surface_whole_yr_mean,
	mul_salinity_surface_whole_yr_mean,
	pan_salinity_surface_whole_yr_mean,
	ser_salinity_surface_whole_yr_mean)
salinity_surface_whole_yr_min=c(dip_salinity_surface_whole_yr_min,
	mul_salinity_surface_whole_yr_min,
	pan_salinity_surface_whole_yr_min,
	ser_salinity_surface_whole_yr_min)

## SALINITY water column whole year
### diplodus salinity water column whole year
dip_salinity_water_column_whole_yr_max=scratch[which(scratch$species=="dip"),]$salinity_5_50_whole_year_max
dip_salinity_water_column_whole_yr_mean=scratch[which(scratch$species=="dip"),]$salinity_5_50_whole_year_mean
dip_salinity_water_column_whole_yr_min=scratch[which(scratch$species=="dip"),]$salinity_5_50_whole_year_min
### mullus salinity water column whole year
mul_salinity_water_column_whole_yr_max=scratch[which(scratch$species=="mul"),]$salinity_0_200_whole_year_max
mul_salinity_water_column_whole_yr_mean=scratch[which(scratch$species=="mul"),]$salinity_0_200_whole_year_mean
mul_salinity_water_column_whole_yr_min=scratch[which(scratch$species=="mul"),]$salinity_0_200_whole_year_min
### serran salinity water column whole year
ser_salinity_water_column_whole_yr_max=scratch[which(scratch$species=="ser"),]$salinity_0_200_whole_year_max
ser_salinity_water_column_whole_yr_mean=scratch[which(scratch$species=="ser"),]$salinity_0_200_whole_year_mean
ser_salinity_water_column_whole_yr_min=scratch[which(scratch$species=="ser"),]$salinity_0_200_whole_year_min
### pan salinity water column whole year
pan_salinity_water_column_whole_yr_max=scratch[which(scratch$species=="pan"),]$salinity_20_90_whole_year_max
pan_salinity_water_column_whole_yr_mean=scratch[which(scratch$species=="pan"),]$salinity_20_90_whole_year_mean
pan_salinity_water_column_whole_yr_min=scratch[which(scratch$species=="pan"),]$salinity_20_90_whole_year_min
### ALL salinity water column whole year
salinity_water_column_whole_yr_max=c(dip_salinity_water_column_whole_yr_max,
	mul_salinity_water_column_whole_yr_max,
	pan_salinity_water_column_whole_yr_max,
	ser_salinity_water_column_whole_yr_max)
salinity_water_column_whole_yr_mean=c(dip_salinity_water_column_whole_yr_mean,
	mul_salinity_water_column_whole_yr_mean,
	pan_salinity_water_column_whole_yr_mean,
	ser_salinity_water_column_whole_yr_mean)
salinity_water_column_whole_yr_min=c(dip_salinity_water_column_whole_yr_min,
	mul_salinity_water_column_whole_yr_min,
	pan_salinity_water_column_whole_yr_min,
	ser_salinity_water_column_whole_yr_min)

## SALINITY SURFACE reproductive period
### diplodus salinity surface reproductive period
dip_salinity_surface_repro_max=scratch[which(scratch$species=="dip"),]$salinity_surface_march_june_max
dip_salinity_surface_repro_mean=scratch[which(scratch$species=="dip"),]$salinity_surface_march_june_mean
dip_salinity_surface_repro_min=scratch[which(scratch$species=="dip"),]$salinity_surface_march_june_min
### mullus salinity surface reproductive period
mul_salinity_surface_repro_max=scratch[which(scratch$species=="mul"),]$salinity_surface_march_july_max
mul_salinity_surface_repro_mean=scratch[which(scratch$species=="mul"),]$salinity_surface_march_july_mean
mul_salinity_surface_repro_min=scratch[which(scratch$species=="mul"),]$salinity_surface_march_july_min
### serran salinity surface reproductive period
ser_salinity_surface_repro_max=scratch[which(scratch$species=="ser"),]$salinity_surface_march_july_max
ser_salinity_surface_repro_mean=scratch[which(scratch$species=="ser"),]$salinity_surface_march_july_mean
ser_salinity_surface_repro_min=scratch[which(scratch$species=="ser"),]$salinity_surface_march_july_min
### pan salinity surface reproductive period
pan_salinity_surface_repro_max=scratch[which(scratch$species=="pan"),]$salinity_surface_june_november_max
pan_salinity_surface_repro_mean=scratch[which(scratch$species=="pan"),]$salinity_surface_june_november_mean
pan_salinity_surface_repro_min=scratch[which(scratch$species=="pan"),]$salinity_surface_june_november_min
### ALL salinity surface reproductive period
salinity_surface_repro_max=c(dip_salinity_surface_repro_max,
	mul_salinity_surface_repro_max,
	pan_salinity_surface_repro_max,
	ser_salinity_surface_repro_max)
salinity_surface_repro_mean=c(dip_salinity_surface_repro_mean,
	mul_salinity_surface_repro_mean,
	pan_salinity_surface_repro_mean,
	ser_salinity_surface_repro_mean)
salinity_surface_repro_min=c(dip_salinity_surface_repro_min,
	mul_salinity_surface_repro_min,
	pan_salinity_surface_repro_min,
	ser_salinity_surface_repro_min)


## SALINITY water column reproductive period
### diplodus salinity water column reproductive period
dip_salinity_water_column_repro_max=scratch[which(scratch$species=="dip"),]$salinity_0_10_march_june_max
dip_salinity_water_column_repro_mean=scratch[which(scratch$species=="dip"),]$salinity_0_10_march_june_mean
dip_salinity_water_column_repro_min=scratch[which(scratch$species=="dip"),]$salinity_0_10_march_june_min
### mullus salinity water column reproductive period
mul_salinity_water_column_repro_max=scratch[which(scratch$species=="mul"),]$salinity_0_10_march_july_max
mul_salinity_water_column_repro_mean=scratch[which(scratch$species=="mul"),]$salinity_0_10_march_july_mean
mul_salinity_water_column_repro_min=scratch[which(scratch$species=="mul"),]$salinity_0_10_march_july_min
### serran salinity water column reproductive period
ser_salinity_water_column_repro_max=scratch[which(scratch$species=="ser"),]$salinity_0_10_march_july_max
ser_salinity_water_column_repro_mean=scratch[which(scratch$species=="ser"),]$salinity_0_10_march_july_mean
ser_salinity_water_column_repro_min=scratch[which(scratch$species=="ser"),]$salinity_0_10_march_july_min
### pan salinity water column reproductive period
pan_salinity_water_column_repro_max=scratch[which(scratch$species=="pan"),]$salinity_60_110_june_november_max
pan_salinity_water_column_repro_mean=scratch[which(scratch$species=="pan"),]$salinity_60_110_june_november_mean
pan_salinity_water_column_repro_min=scratch[which(scratch$species=="pan"),]$salinity_60_110_june_november_min
### ALL salinity water column reproductive period
salinity_water_column_repro_max=c(dip_salinity_water_column_repro_max,
	mul_salinity_water_column_repro_max,
	pan_salinity_water_column_repro_max,
	ser_salinity_water_column_repro_max)
salinity_water_column_repro_mean=c(dip_salinity_water_column_repro_mean,
	mul_salinity_water_column_repro_mean,
	pan_salinity_water_column_repro_mean,
	ser_salinity_water_column_repro_mean)
salinity_water_column_repro_min=c(dip_salinity_water_column_repro_min,
	mul_salinity_water_column_repro_min,
	pan_salinity_water_column_repro_min,
	ser_salinity_water_column_repro_min)

## TEMPERATURE whole yr
### diplodus temperature whole yr
dip_sst_whole_yr_max=scratch[which(scratch$species=="dip"),]$temperature_surface_whole_year_max
dip_sst_whole_yr_mean=scratch[which(scratch$species=="dip"),]$temperature_surface_whole_year_mean
dip_sst_whole_yr_min=scratch[which(scratch$species=="dip"),]$temperature_surface_whole_year_min
### mullus temperature whole yr
mul_sst_whole_yr_max=scratch[which(scratch$species=="mul"),]$temperature_surface_whole_year_max
mul_sst_whole_yr_mean=scratch[which(scratch$species=="mul"),]$temperature_surface_whole_year_mean
mul_sst_whole_yr_min=scratch[which(scratch$species=="mul"),]$temperature_surface_whole_year_min
### serran temperature whole yr
ser_sst_whole_yr_max=scratch[which(scratch$species=="ser"),]$temperature_surface_whole_year_max
ser_sst_whole_yr_mean=scratch[which(scratch$species=="ser"),]$temperature_surface_whole_year_mean
ser_sst_whole_yr_min=scratch[which(scratch$species=="ser"),]$temperature_surface_whole_year_min
### pan temperature whole yr
pan_sst_whole_yr_max=scratch[which(scratch$species=="pan"),]$temperature_surface_whole_year_max
pan_sst_whole_yr_mean=scratch[which(scratch$species=="pan"),]$temperature_surface_whole_year_mean
pan_sst_whole_yr_min=scratch[which(scratch$species=="pan"),]$temperature_surface_whole_year_min
### ALL temperature whole yr
sst_whole_yr_max=c(dip_sst_whole_yr_max,
	mul_sst_whole_yr_max,
	pan_sst_whole_yr_max,
	ser_sst_whole_yr_max)
sst_whole_yr_mean=c(dip_sst_whole_yr_mean,
	mul_sst_whole_yr_mean,
	pan_sst_whole_yr_mean,
	ser_sst_whole_yr_mean)
sst_whole_yr_min=c(dip_sst_whole_yr_min,
	mul_sst_whole_yr_min,
	pan_sst_whole_yr_min,
	ser_sst_whole_yr_min)

## TEMPERATURE reproductive period
### diplodus temperature reprodutive period
dip_sst_repro_max=scratch[which(scratch$species=="dip"),]$temperature_surface_march_june_max
dip_sst_repro_mean=scratch[which(scratch$species=="dip"),]$temperature_surface_march_june_mean
dip_sst_repro_min=scratch[which(scratch$species=="dip"),]$temperature_surface_march_june_min
### mullus temperature reprodutive period
mul_sst_repro_max=scratch[which(scratch$species=="mul"),]$temperature_surface_march_july_max
mul_sst_repro_mean=scratch[which(scratch$species=="mul"),]$temperature_surface_march_july_mean
mul_sst_repro_min=scratch[which(scratch$species=="mul"),]$temperature_surface_march_july_min
### serran temperature reprodutive period
ser_sst_repro_max=scratch[which(scratch$species=="ser"),]$temperature_surface_march_july_max
ser_sst_repro_mean=scratch[which(scratch$species=="ser"),]$temperature_surface_march_july_mean
ser_sst_repro_min=scratch[which(scratch$species=="ser"),]$temperature_surface_march_july_min
### pan temperature reprodutive period
pan_sst_repro_max=scratch[which(scratch$species=="pan"),]$temperature_surface_june_november_max
pan_sst_repro_mean=scratch[which(scratch$species=="pan"),]$temperature_surface_june_november_mean
pan_sst_repro_min=scratch[which(scratch$species=="pan"),]$temperature_surface_june_november_min
### ALL temperature reproductive period
sst_repro_max=c(dip_sst_repro_max,
	mul_sst_repro_max,
	pan_sst_repro_max,
	ser_sst_repro_max)
sst_repro_mean=c(dip_sst_repro_mean,
	mul_sst_repro_mean,
	pan_sst_repro_mean,
	ser_sst_repro_mean)
sst_repro_min=c(dip_sst_repro_min,
	mul_sst_repro_min,
	pan_sst_repro_min,
	ser_sst_repro_min)

## CHLOROPHYLL benthic
### diplodus chlorophyll benthic
dip_chlo_benthic_max=scratch[which(scratch$species=="dip"),]$chloBenthicMax
dip_chlo_benthic_mean=scratch[which(scratch$species=="dip"),]$chloBenthicMean
dip_chlo_benthic_min=scratch[which(scratch$species=="dip"),]$chloBenthicMin
### mullus chlorophyll benthic
mul_chlo_benthic_max=scratch[which(scratch$species=="mul"),]$chloBenthicMax
mul_chlo_benthic_mean=scratch[which(scratch$species=="mul"),]$chloBenthicMean
mul_chlo_benthic_min=scratch[which(scratch$species=="mul"),]$chloBenthicMin
### serran chlorophyll benthic
ser_chlo_benthic_max=scratch[which(scratch$species=="ser"),]$chloBenthicMax
ser_chlo_benthic_mean=scratch[which(scratch$species=="ser"),]$chloBenthicMean
ser_chlo_benthic_min=scratch[which(scratch$species=="ser"),]$chloBenthicMin
### pan chlorophyll benthic
pan_chlo_benthic_max=scratch[which(scratch$species=="pan"),]$chloBenthicMax
pan_chlo_benthic_mean=scratch[which(scratch$species=="pan"),]$chloBenthicMean
pan_chlo_benthic_min=scratch[which(scratch$species=="pan"),]$chloBenthicMin
### ALL chlorophyll benthic
chlo_benthic_max=c(dip_chlo_benthic_max,
	mul_chlo_benthic_max,
	pan_chlo_benthic_max,
	ser_chlo_benthic_max)
chlo_benthic_mean=c(dip_chlo_benthic_mean,
	mul_chlo_benthic_mean,
	pan_chlo_benthic_mean,
	ser_chlo_benthic_mean)
chlo_benthic_min=c(dip_chlo_benthic_min,
	mul_chlo_benthic_min,
	pan_chlo_benthic_min,
	ser_chlo_benthic_min)

## CHLOROPHYLL surface
### diplodus chlorophyll surface
dip_chlo_surface_max=scratch[which(scratch$species=="dip"),]$chloSurfaceMax
dip_chlo_surface_mean=scratch[which(scratch$species=="dip"),]$chloSurfaceMean
dip_chlo_surface_min=scratch[which(scratch$species=="dip"),]$chloSurfaceMin
### mullus chlorophyll surface
mul_chlo_surface_max=scratch[which(scratch$species=="mul"),]$chloSurfaceMax
mul_chlo_surface_mean=scratch[which(scratch$species=="mul"),]$chloSurfaceMean
mul_chlo_surface_min=scratch[which(scratch$species=="mul"),]$chloSurfaceMin
### serran chlorophyll surface
ser_chlo_surface_max=scratch[which(scratch$species=="ser"),]$chloSurfaceMax
ser_chlo_surface_mean=scratch[which(scratch$species=="ser"),]$chloSurfaceMean
ser_chlo_surface_min=scratch[which(scratch$species=="ser"),]$chloSurfaceMin
### pan chlorophyll surface
pan_chlo_surface_max=scratch[which(scratch$species=="pan"),]$chloSurfaceMax
pan_chlo_surface_mean=scratch[which(scratch$species=="pan"),]$chloSurfaceMean
pan_chlo_surface_min=scratch[which(scratch$species=="pan"),]$chloSurfaceMin
### ALL chlorophyll surface
chlo_surface_max=c(dip_chlo_surface_max,
	mul_chlo_surface_max,
	pan_chlo_surface_max,
	ser_chlo_surface_max)
chlo_surface_mean=c(dip_chlo_surface_mean,
	mul_chlo_surface_mean,
	pan_chlo_surface_mean,
	ser_chlo_surface_mean)
chlo_surface_min=c(dip_chlo_surface_min,
	mul_chlo_surface_min,
	pan_chlo_surface_min,
	ser_chlo_surface_min)


final=data.frame(labels=labels,
	species=species,
	salinity_surface_whole_yr_max=salinity_surface_whole_yr_max,
	salinity_surface_whole_yr_mean=salinity_surface_whole_yr_mean,
	salinity_surface_whole_yr_min=salinity_surface_whole_yr_min,
	salinity_water_column_whole_yr_max=salinity_water_column_whole_yr_max,
	salinity_water_column_whole_yr_mean=salinity_water_column_whole_yr_mean,
	salinity_water_column_whole_yr_min=salinity_water_column_whole_yr_min,
	salinity_surface_repro_max=salinity_surface_repro_max,
	salinity_surface_repro_mean=salinity_surface_repro_mean,
	salinity_surface_repro_min=salinity_surface_repro_min,
	salinity_water_column_repro_max=salinity_water_column_repro_max,
	salinity_water_column_repro_mean=salinity_water_column_repro_mean,
	salinity_water_column_repro_min=salinity_water_column_repro_min,
	sst_whole_yr_max=sst_whole_yr_max,
	sst_whole_yr_mean=sst_whole_yr_mean,
	sst_whole_yr_min=sst_whole_yr_min,
	sst_repro_max=sst_repro_max,
	sst_repro_mean=sst_repro_mean,
	sst_repro_min=sst_repro_min,
	chlo_surface_max=chlo_surface_max,
	chlo_surface_mean=chlo_surface_mean,
	chlo_surface_min=chlo_surface_min,
	chlo_benthic_max=chlo_benthic_max,
	chlo_benthic_mean=chlo_benthic_mean,
	chlo_benthic_min=chlo_benthic_min)

write.table(final, file="03-results/final_chloro_temperature_salinity.tsv",quote=F,sep="\t",col.names=T,row.names=F)






