## Carte Salinit? Max
library(raster)
library(ggplot2)
library(ggmap)
library(rasterVis)
library(rgdal)

#### Download data
load("data_sal_Max")
data_sal_Max = data.frame(data_sal_Max)
crs.geo <- CRS("+proj=longlat +datum=WGS84 +no_defs +ellps=WGS84 +towgs84=0,0,0")  # geographical, datum WGS84
coordinates(data_sal_Max) = c("long", "lat")
proj4string(data_sal_Max) <- crs.geo  # define projection system of our data

Smax = SpatialPixelsDataFrame(as(data_sal_Max, "SpatialPoints"), data=as(data_sal_Max, "data.frame"), tolerance=0.9)
Smax.df <- as.data.frame(Smax)

gridded(Smax)=TRUE
plot(Smax)
r_Smax=raster(Smax, layer="res", values=T)
s <- stack(r_Smax, r_Smax*2)
#layerNames(s) <- c('meuse', 'meuse x 2')
projection(r_Smax) = CRS(proj4string(Smax))
OR<-readOGR(dsn="/Users/laura/Desktop/", layer="ne_110m_land.shp")

ggplot(Smax.df) +  
  geom_tile(aes(x=long, y=lat,fill=factor(res),alpha=0.8)) + 
  geom_polygon(data=OR, aes(x=long, y=lat, group=group), 
               fill=NA,color="grey50", size=1)

#open ASCII file using ‘raster’ command, which converts the ASCII to a raster object
load("01-infos/data_sal_Max")
map = data.frame(data_sal_Max)

#convert the raster to points for plotting
map.p <- rasterToPoints(map)

#Make the points a dataframe for ggplot
df <- data.frame(map.p)

#Make appropriate column headings
colnames(df) <- c("Longitude", "Latitude")

#Call in point data, in this case a fake transect (csv file with lat and lon coordinates)
sites <- data.frame(read.csv(“/your/path/to/pointfile.csv”))

#Now make the map
ggplot(data=map, aes(y=lat, x=long)) +
  geom_raster(aes(fill=res)) +
  geom_point(data=sites, aes(x=x, y=y), color=”white”, size=3, shape=4) +
  theme_bw() +
  coord_equal() +
  scale_fill_gradient(“MAP (mm/yr)”, limits=c(0,2500)) +
  theme(axis.title.x = element_text(size=16),
        axis.title.y = element_text(size=16, angle=90),
        axis.text.x = element_text(size=14),
        axis.text.y = element_text(size=14),
        panel.grid.major = element_blank(),
        panel.grid.minor = element_blank(),
        legend.position = “right”,
        legend.key = element_blank()
  )