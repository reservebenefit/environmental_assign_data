Scripts to assign environmental descriptors to samples 
======================================================
2019

# Table of contents

1. [Introduction](#1-introduction)
2. [Installation](#2-installation)
	1. [Prerequisites](#21-prerequisite)
	2. [Data files](#22-data_files)
3. [Reporting bugs](#3-reporting-bugs)
4. [Running the pipeline](#4-running-the-pipeline)
5. [Results](#5-results)

# 1. Introduction

R scripts to assign environmental data to samples according to their coordinates.

# 2. Installation

open a shell
make a folder, name it yourself, I named it workdir
```
mkdir workdir
cd workdir
```
clone the project and switch to the main folder, it's your working directory
```
git clone https://gitlab.mbb.univ-montp2.fr/reservebenefit/environmental_assign_data.git
cd environmental_assign_data
```

## 2.1 Prerequisites
You must install the following softwares and packages :

- [R Version 3.2.3](https://cran.r-project.org/) to run the scripts
    - `R-package` raster
    - `R-package` rgdal
    - `R-package` ncdf4

- [FILEZILLA](https://filezilla-project.org/download.php) to download environmental data from our private SFTP server

## 2.2 Data files
- The included data files are :

	* [01-sample_coords/all_coords.csv](01-sample_coords/all_coords.csv) : label and WGS84 coordinates of all the samples from RESERVEBENEFIT

- You must download the following environmental descriptor raw data into the folder [01-infos](01-infos) :

You will need [FILEZILLA](https://filezilla-project.org/download.php) !
To access to our private server, please follow these instructions

1) Launch [FILEZILLA](https://filezilla-project.org/download.php)
2) click on file > import...
3) select [22-server-infos/SFTPCEFE_PEG.xml](22-server-infos/SFTPCEFE_PEG.xml)
4) click on the site manager button (the button under "File")

![site manager button](22-server-infos/image1.png)

5) select site "CEFE-STFP" and then click on "Connect"

![site manager button](22-server-infos/image2.png)

Now you are connected to our server and you can download the required files. They are stored into : /DataEnvironment/resources

Download the content of /DataEnvironment/resources and copy it into the folder [01-infos](01-infos). Your data should be stored this way :

```
01-infos/
├── chlorophyll
│   ├── benthic
│   │   ├── Present.Benthic.Mean.Depth.Chlorophyll.Max.tif.zip
│   │   ├── Present.Benthic.Mean.Depth.Chlorophyll.Mean.tif.zip
│   │   └── Present.Benthic.Mean.Depth.Chlorophyll.Min.tif.zip
│   └── surface
│       ├── Present.Surface.Chlorophyll.Max.tif.zip
│       ├── Present.Surface.Chlorophyll.Mean.tif.zip
│       └── Present.Surface.Chlorophyll.Min.tif.zip
├── habitat
│   ├── EUSeaMap2016_substrate_levels_med_res200m.tsv
│   └── EUSeaMap2016_substrate_raster_med_res200m.tif
├── salinity_et_temperature
    └── CMEMS-MedSea_Reanalysis_Phys_006_004_0to210m_1987-2015_1m_votemper_vosaline_SampleLoc.nc
```

### 2.2.a data file sources

* Chlorophyll : https://www.bio-oracle.org
* Habitat     : https://www.emodnet-seabedhabitats.eu
* Salinity    : GEOMAR
* Temperature : GEOMAR

# 3. Reporting bugs

If you're sure you've found a bug — e.g. if one of my programs crashes
with an obscur error message, or if the resulting file is missing part
of the original data, then by all means submit a bug report.

I use [GitLab's issue system](https://gitlab.mbb.univ-montp2.fr/reservebenefit/environmental_assign_data/issues)
as my bug database. You can submit your bug reports there. Please be as
verbose as possible — e.g. include the command line, etc

# 4. Running the pipeline

:point_right: You must have download required data files into [01-infos](01-infos) (see [Data files](#22-data_files) section)

### 4.1. Assign `habitat` (substrat) values to samples

To attribute `habitat` information (this will generate [03-results/all_habitat.tsv](03-results/all_habitat.tsv))
```
Rscript 00-scripts/habitat.R
```

### 4.2. Assign `chlorophyll A abundance` values to samples
To attribute "chlorophyll A" information (this will generate [03-results/all_habitat.tsv](03-results/all_chlorophyll.tsv))
```
Rscript 00-scripts/chlorophyll.R

```

### 4.3. Assign `mounthly temperature & salinity` values to samples for each sea depth

To attribute `mounthly temperature & salinity` by sea depth information (this will generate [03-results/all_depth_repro_temperature_salinity.tsv](03-results/all_depth_repro_temperature_salinity.tsv))
```
Rscript 00-scripts/salt_temp.R
```


### 4.4. Correct environmental data values

* Correct `mounthly temperature & salinity` values

:warning: `mounthly temperature & salinity` values for these following samples were missing data and then have been **_manually_** fullfiled:  
(ce serait bien de savoir _COMMENT ?_)
```
mul_bal_115
mul_bal_116
mul_bal_117
mul_bal_118
mul_bal_119
mul_bal_120
mul_bal_121
mul_bal_122
mul_bal_123
mul_bal_124
mul_bal_125
mul_bal_126
mul_bal_127
mul_bal_128
mul_bal_129
mul_bal_130
mul_bal_131
```
(see file [03-results/all_depth_repro_CORRECTED_temperature_salinity.csv](03-results/03-results/all_depth_repro_CORRECTED_temperature_salinity.csv))





### 4.5. All environmental descriptors into one single table

To merge all data into one single table (this will generate [03-results/final_chloro_temperature_salinity.tsv](03-results/final_chloro_temperature_salinity.tsv)
```
Rscript 00-scripts/merge_table.R
```


# 5. Results

* [03-results/all_habitat.tsv](03-results/all_habitat.tsv) : table of samples with WGS84 coordinates and predicted habitat
* [03-results/all_CHECKED_chlorophyll.tsv](03-results/all_CHECKED_chlorophyll.tsv) : table of samples with WGS84 coordinates and chlorophyll A - benthic and surface - mean,max,min values
* [03-results/final_chloro_temperature_salinity.tsv](03-results/final_chloro_temperature_salinity.tsv) :  table of samples with WGS84 coordinates and all the environmental data (predicted habitat, chlorophyll A, temperature, salinity by mounth and by depth values)
